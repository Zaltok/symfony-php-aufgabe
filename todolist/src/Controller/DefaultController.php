<?php

namespace App\Controller;

use App\Entity\Task;
use App\Entity\User;
use App\Form\TaskType;
use App\Form\UserFormType;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\PasswordType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Mailer\MailerInterface;
use Symfony\Component\Mime\Email;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Security\Core\Encoder\UserPasswordEncoderInterface;

class DefaultController extends AbstractController
{
    /**
     * @Route("/", name="default")
     */
    public function index(): Response
    {
        if($this->getUser()) {
            return $this->redirect('/dashboard');
        }
        return $this->redirect($this->generateUrl('app_login'));
    }

    /**
     * @Route ("/dashboard", name="app_dashboard")
     * @return Response
     */
    public function dashboard() : Response {
        if(!$this->getUser()) {
            return $this->redirect($this->generateUrl('app_login'));
        }
        return $this->render('default/dashboard.html.twig');
    }

    /**
     * @Route("/tasks", name="app_tasklist")
     * @return Response
     */
    public function tasklist()
    {
        if (!$this->getUser()) {
            return $this->redirect($this->generateUrl('app_login'));
        }
        $manager = $this->getDoctrine()->getManager();
        $tasks = $manager->getRepository(Task::class)->findBy([], ['createdAt' => 'DESC']);
        return $this->render('default/tasklist.html.twig', ['tasks' => $tasks]);
    }

    /**
     * @Route("/users", name="app_users")
     * @return Response
     */
    public function userslist(Request $request)
    {
        if(!$this->getUser()) {
            return $this->redirect($this->generateUrl('app_login'));
        }
        $manager = $this->getDoctrine()->getManager();
        $users = $manager->getRepository(User::class)->findAll();

        return $this->render('default/userlist.html.twig', ['users' => $users]);
    }

    /**
     * @Route("/users/add", name="app_users_add")
     * @return Response
     */
    public function userform(Request $request,UserPasswordEncoderInterface $passwordEncoder)
    {
        if(!$this->getUser() || true) {
            return $this->redirect($this->generateUrl('app_login'));
        }/*
        $user = new User();
        $manager = $this->getDoctrine()->getManager();
        $form = $this->createForm(UserFormType::class, $user);
        $form->add('save', SubmitType::class);

        $form->handleRequest($request);
        if($form->isSubmitted() && $form->isValid()) {
            $user = $form->getData();
            $user->setPassword($passwordEncoder->encodePassword($user, $user->getPassword()));
            $this->getDoctrine()->getManager()->persist($user);
            $this->getDoctrine()->getManager()->flush();
            return $this->redirect($this->generateUrl('app_users'));
        }*/
        return $this->render('default/default.html.twig');//['form' => $form->createView()]);
    }

    /**
     * @Route("/tasks/add", name="app_tasks_add")
     * @return Response
     */
    public function taskform(Request $request,UserPasswordEncoderInterface $passwordEncoder)
    {
        if(!$this->getUser()) {
            return $this->redirect($this->generateUrl('app_login'));
        }
        $task = new Task();
        $manager = $this->getDoctrine()->getManager();
        $form = $this->createForm(TaskType::class, $task);
        $form->add('save', SubmitType::class);

        $form->handleRequest($request);
        if($form->isSubmitted() && $form->isValid()) {
            /** @var Task $task */
            $task = $form->getData();
            $task->setStatus(0);
            $task->setCreatedAt(new \DateTime('now'));
            $task->setCreatedBy($this->getUser());
            $manager->persist($task);
            $manager->flush();
            return $this->redirect($this->generateUrl('app_tasklist'));
        }
        return $this->render('default/default.html.twig', ['title' => 'Add new Task', 'form' => $form->createView()]);
    }

    /**
     * @Route("/tasks/edit/{id}", name="app_tasks_edit")
     * @return Response
     */
    public function edittask(Request $request, MailerInterface $mailer, $id)
    {
        if(!$this->getUser()) {
            return $this->redirect($this->generateUrl('app_login'));
        }
        $manager = $this->getDoctrine()->getManager();
        /** @var Task $task */
        $task = $manager->getRepository(Task::class)->find($id);
        $tmpAssignedTo = $task->getAssigned();
        $form = $this->createForm(TaskType::class, $task);
        $form->add('status', ChoiceType::class, ['choices' => ['Open' => 0, 'In Progress' => 1, 'Pending' => 2, 'Closed' => 3]]);
        $form->add('save', SubmitType::class);

        $form->handleRequest($request);
        if($form->isSubmitted() && $form->isValid()) {
            $task = $form->getData();
            if($task->getAssigned() !== $tmpAssignedTo) {
                $mail = new Email();
                $mail->to($task->getAssigned()->getEmail() ?? 'no-reply@bysaeth.de')
                    ->from('no-reply@todolist.example')
                    ->subject('TodoList: Neue Ticketzuweisung')
                    ->text("Ihnen wurde ein Ticket zugewiesen");
                $mailer->send($mail);

            }
            $manager->persist($task);
            $manager->flush();
            return $this->redirect($this->generateUrl('app_tasklist'));
        }
        return $this->render('default/default.html.twig', ['title' => 'Add new Task', 'form' => $form->createView()]);
    }
}
