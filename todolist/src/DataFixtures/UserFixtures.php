<?php

namespace App\DataFixtures;

use App\Entity\User;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Bundle\FixturesBundle\FixtureGroupInterface;
use Doctrine\Persistence\ObjectManager;
use Symfony\Component\Security\Core\Encoder\UserPasswordEncoderInterface;

class UserFixtures extends Fixture implements FixtureGroupInterface
{
    private $encoder;

    public function __construct(UserPasswordEncoderInterface $passwordEncoder)
    {
        $this->encoder = $passwordEncoder;
    }

    public static function getGroups(): array
    {
        return ['fi06'];
    }

    public function load(ObjectManager $manager)
    {
        $minDate = "2021-02-01";
        $users = [
            'fi06', 'bbysaeth', 'jadams', 'aberent', 'vbobylev',
            'sbruns', 'ubulut','cbundfuss', 'adesouzavieira',
            'adornbusch', 'hduennebacke', 'ffuehring', 'lguenther', 'fhadisoudarjani', 'ahaebell',
            'zhakami', 'kluenert', 'mniklas', 'jpintat', 'lrodriguezrichardson', 'zsalihi', 'csandjotchana', 'mwinkelstraeter'];
        foreach($users as $username) {
            $user = new User();
            $user->setUsername($username);
            $user->setFirstname(strtoupper(substr($username, 0, 1)));
            $user->setLastname(ucfirst(substr($username, 1)));
            $user->setEmail($username. '@bysaeth.de');
            $user->setPassword($this->encoder->encodePassword($user, $username));
            $user->setStreet('');
            $user->setPostal('');
            $user->setCity('');
            $user->setRegisteredAt($this->randomDate($minDate, date("Y-m-d")));
            $user->setEnabled(rand(0,1));
            $user->setActivationCode('');
            $manager->persist($user);
        }

        $manager->flush();
    }

    private function randomDate($start, $end) {
        // Convert to timetamps
        $min = strtotime($start);
        $max = strtotime($end);

        $randomDate = mt_rand($min, $max);

        return \DateTime::createFromFormat('U',$randomDate);
    }
}
