<?php

namespace App\DataFixtures;

use App\Entity\Task;
use App\Entity\User;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Bundle\FixturesBundle\FixtureGroupInterface;
use Doctrine\Common\DataFixtures\DependentFixtureInterface;
use Doctrine\Persistence\ObjectManager;

class TaskFixture extends Fixture implements FixtureGroupInterface,DependentFixtureInterface
{

    public function load(ObjectManager $manager)
    {
        $maxDate = "2021-12-31";
        $minDate = "2019-01-01";
        $users = $manager->getRepository(User::class)->findAll();
        for($i = 0; $i < 100; $i++) {
            shuffle($users);
            $task = new Task();
            $task->setTitle("Task: This is a sample.");
            $task->setStatus(rand(1,4));
            $task->setCreatedBy($users[0]);
            shuffle($users);
            $task->setAssigned($users[0] !== $task->getCreatedBy() ? $users[0] : null);
            $task->setCreatedAt($this->randomDate($minDate, date("Y-m-d")));
            $task->setDueDate($this->randomDate($task->getCreatedAt()->format('Y-m-d'), $maxDate));
            $manager->persist($task);
        }

        $manager->flush();
    }

    public static function getGroups(): array
    {
        return ['fi06'];
    }

    private function randomDate($start, $end) {
        // Convert to timetamps
        $min = strtotime($start);
        $max = strtotime($end);

        $randomDate = mt_rand($min, $max);

        return \DateTime::createFromFormat('U',$randomDate);
    }

    public function getDependencies()
    {
        return [UserFixtures::class];
    }
}
